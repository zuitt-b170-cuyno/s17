const students = []

function addStudent(student) {
    student = capitalize(student)
    students.push(student)
    console.log(`${student} was added to the student's list`)
}

function countStudent() {
    const numStudent = students.length
    console.log(`There are a total of ${numStudent} students enrolled`)
}

function printStudents() {
    students.forEach(student => console.log(student))
}

function findStudent(person) {
    person = capitalize(person)
    const list = students.filter(student => student.indexOf(person) === 0), numStudent = list.length
   
    if (numStudent > 1) 
        console.log(`${list.join(", ")} are enrollees`)
    else if (numStudent === 1)
        console.log(`${list[0]} is an enrollee`)
    else 
        console.log(`${person} is not an enrollee`)
}

function removeStudent(student) {
    student = capitalize(student)
    if (students.includes(student)) {
        const index = students.indexOf(student)
        students.splice(index, 1)
        console.log(`${student} was removed from student's list`)
    }
}

function addSection(section) {
    const length = students.length
    if (length) console.log(students.map(student => `${student} - section ${section}`))
}

function capitalize(string) {
    return string[0].toUpperCase() + string.slice(1)
}