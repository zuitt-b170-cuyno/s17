const task = ["drink HTML", "eat Javascript", "inhale CSS", "bake Bootstrap"]
console.log(task)
for (let i in task) 
    console.log(task[i])

console.log(task[2])
console.log(task.length)
const lastIndex = task.length - 1

// Add Element
const number = ["one", "two", "three", "four"]
console.log(number)
number[4] = "five"
console.log(number)

// push Method - add element at the end of the array
number.push("element")
console.log(number)

// callback function - function passed as an argument of another function
function pushMethod(element) { number.push(element) }
pushMethod("six")
pushMethod("seven")
pushMethod("eight")
console.log(number)

// pop Method - remove the last element of an array
const lastElement = number.pop()
console.log(lastElement)
console.log(number)
function popMethod() { number.pop() }
popMethod()

// shift method = removes the first element of an array
number.shift()
console.log(number)
function shiftMethod() { number.shift() }
shiftMethod()
console.log(number)

// unshift method - add element at the beginning of an array
number.unshift("zero")
console.log(number)

function unshiftMethod(element) { number.unshift(element) }
unshiftMethod("mcdo")
console.log(number)

// sort method - sort the elements of the array
const numbers = [15, 17, 32, 12, 6, 8, 236]
numbers.sort(function (a, b) { return a - b } )
console.log(numbers)

numbers.sort(function (a, b) { return b - a } )
console.log(numbers)

// reverse method - reverse the order of the elements in an array
numbers.reverse()
console.log(numbers)

// splice method - removes/add elements in the middle of an array
/* 
    Delete
    first parameter - starting point of deletion
    second parameter - number of elements to be removed. If not present, all elements 
        starting from the index specified by the first parameter will be deleted.

    Add
    first parameter - starting point of addition
    second parameter - number of elements to be added.
    the rest - replacement
*/
// const nums = numbers.splice(4, 2, 31, 11)
// console.log(numbers)
// console.log(nums)

// let slicedNums = numbers.slice(1)
// console.log(slicedNums)

const slicedNums = numbers.slice(4, 6)
console.log(numbers)
console.log(slicedNums)

console.log(numbers)
console.log(number)

const animal = ["dog", "tiger", "kangaroo"]
const newConcat = numbers.concat(number, animal)
console.log(newConcat)

const meal = ["rice", "steak", "juice"]
console.log(meal.join(", "))

console.log(numbers)
console.log(typeof numbers[3])

const newString = numbers.toString()
console.log(newString)
console.log(typeof newString)

// Accessors
const countries = ["US", "PH", "JP", "HK", "SG", "PH", "NZ"]
const index1 = countries.indexOf("PH"), index2 = countries.lastIndexOf("PH")
console.log(index1)
console.log(index2)

// Iterators - Higher-order function
// forEach - iterate each element in an array

const days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
days.forEach(function (element) {
    console.log(element)
})

// Map

const mapDays = days.map(function (element) {
    return `${element} is the day of the week`
})

console.log(mapDays)

// Filter
console.log(numbers)
const newFilter = numbers.filter(function(element) { return element < 30} )
console.log(newFilter)

// Includes
const animalIncludes = animal.includes("dog")
console.log(animalIncludes)

// Every
const newEvery = numbers.every(function (element) {
    return element > 10
})

console.log(numbers)
console.log(newEvery)

// Some
const newSome = numbers.some(function (element) {
    return element > 10
})

console.log(newSome)

// Reduce
const newReduce = numbers.reduce(function (a, b) {
    return a + b
})

console.log(newReduce)
const average = newReduce / numbers.length

// toFixed
console.log(average.toFixed(2))
console.log(parseInt(average.toFixed(2)))
console.log(parseFloat(average.toFixed(2)))